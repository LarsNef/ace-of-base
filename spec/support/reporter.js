const fs = require('fs');
const path = require('path');

class CustomReporter {
    constructor() {
        this.result = [];
    }

    makeSuite(data) {
        const newParent = this.suite;

        this.suite = {
            data: data || {},
            specs: [],
            children: [],
            parent: this.suite || null
        };

        if (newParent) {
            newParent.children.push(this.suite);
        }
    }

    jasmineStarted(suiteInfo) {
        console.log(`Running suite with ${suiteInfo.totalSpecsDefined}`);
    }

    suiteStarted(suite) {
        this.makeSuite(suite);

        console.log(`Running suite ${suite.description} with the full description of ${suite.fullName}`);
    }

    specStarted(result) {
        console.log(`Running spec ${result.description} with the full description of ${result.fullName}`)
    }

    specDone(spec) {
        console.log(`Spec ${spec.description} was ${spec.status}`);

        for (const failure of spec.failedExpectations) {
            console.log(`Failure: ${failure.message}`);
            console.log(failure.stack);
        }

        console.log(`${spec.passedExpectations.length} test passed.`);

        this.suite.specs.push(spec);
    }

    suiteDone(result) {
        console.log(`${result.description} was ${result.status}`);

        for (const failure of result.failedExpectations) {
            console.log(`Suite ${failure.message}`);
            console.log(failure.stack);
        }

        if (this.suite.parent) {
            this.suite = this.suite.parent;
        } else {
            this.result.push(this.suite);
            this.suite = null;
        }
    }

    jasmineDone(result) {
        console.log(`Finished suite: ${result.overallStatus}`);

        for (const failure of result.failedExpectations) {
            console.log(`Global ${failure.message}`);
            console.log(failure.stack);
        }

        const suites = [];
        const stack = [...this.result];
        while (stack.length > 0) {
            const suite = stack.pop();
            if (suite.parent) {
                suite.parent = suite.parent.data.id;
            } else {
                delete suite.parent;
            }

            for (const child of suite.children) {
                stack.push(child);
            }

            delete suite.children;
            suites.push(suite);
        }

        const resultDir = path.join('build', 'test');
        if (!fs.existsSync(resultDir)) {
            const mkdirs = fs.mkdirSync(resultDir, {
                recursive: true
            });
        }

        fs.writeFileSync(
            path.join(resultDir, `jasmine-test-report.json`), // ${new Date().getTime()}
            JSON.stringify(suites, null, '  '),
            {
                encoding: 'utf-8'
            }
        )
    }
}

module.exports = CustomReporter;
